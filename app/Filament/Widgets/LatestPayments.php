<?php

namespace App\Filament\Widgets;

use App\Models\Payment;
use Closure;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Widgets\TableWidget as BaseWidget;
use Illuminate\Database\Eloquent\Builder;

class LatestPayments extends BaseWidget
{
    protected static ?int $sort = 3;
//    protected int | string | array $columnSpan = "full";

    protected function getTableQuery(): Builder
    {
        return Payment::with('product')->latest()->take(5);
    }

    protected function getTableColumns(): array
    {
        return [
            TextColumn::make('created_at')->label('Time'),
            TextColumn::make('total')->money('usd'),
            TextColumn::make('product.name'),
        ];
    }
    protected function isTablePaginationEnabled(): bool
    {
        return false;
    }
}
