<?php

namespace App\Filament\Pages;

use Filament\Pages\Page;

class About extends Page
{
    protected static ?string $navigationIcon = 'heroicon-o-document-text';

    protected static string $view = 'filament.pages.about';



    protected static function getNavigationLabel(): string
    {
        return __('About');
    }
    protected function getTitle(): string
    {
        return __('About');
    }
//    public static function getName(): string
//    {
//        return __('about');
//    }


}
