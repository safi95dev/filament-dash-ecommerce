<?php

namespace App\Providers;

use App\Filament\Pages\About;
use App\Filament\Resources\PaymentResource;
use App\Filament\Resources\ProductResource;
use App\Filament\Resources\TagResource;
use App\Filament\Resources\UserResource;
use App\Filament\Resources\VoucherResource;
use Filament\Facades\Filament;
use Filament\Navigation\NavigationGroup;
use Filament\Pages\Dashboard;
use Illuminate\Support\ServiceProvider;
use Filament\Navigation\NavigationBuilder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Filament::navigation(function (NavigationBuilder $builder): NavigationBuilder {
            return $builder->items([
                ...Dashboard::getNavigationItems(),
            ])
                ->groups([
                    NavigationGroup::make('shop')->label(__('shop'))
                        ->items([
                            ...PaymentResource::getNavigationItems(),
                            ...UserResource::getNavigationItems(),
                            ...ProductResource::getNavigationItems(),
                            ...VoucherResource::getNavigationItems(),
                            ...TagResource::getNavigationItems(),
                        ]),
                    NavigationGroup::make('other')->label(__('other'))->collapsed()
                        ->items([
                            ...About::getNavigationItems(),
                        ]),
                ]);
        });
    }
}
