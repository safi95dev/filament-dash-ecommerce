<?php

namespace App\Filament\Resources;

use App\Filament\Resources\PaymentResource\Pages;
use App\Filament\Resources\PaymentResource\RelationManagers;
use App\Models\Payment;
use App\Models\User;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class PaymentResource extends Resource
{
    protected static ?string $model = Payment::class;

    protected static ?string $navigationIcon = 'heroicon-o-currency-dollar';

    public static function getPluralLabel(): string
    {
        return __('Payments');
    }


    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                //
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('created_at')->sortable()->date('d/m/Y H:i')->label('Payment time'),
                Tables\Columns\TextColumn::make('product.name')->sortable()
                    ->url(fn(Payment $record) => ProductResource::getUrl('edit', $record->product)),
                Tables\Columns\TextColumn::make('user.name')->sortable()->label('User name')
                    ->url(fn(Payment $record) => UserResource::getUrl('edit', $record->user)),
                Tables\Columns\TextColumn::make('user.email')->sortable()->label('User email'),
                Tables\Columns\TextColumn::make('voucher.code')->sortable(),
                Tables\Columns\TextColumn::make('subtotal')->sortable()->money('usd'),
                Tables\Columns\TextColumn::make('taxes')->sortable()->money('usd'),
                Tables\Columns\TextColumn::make('total')->sortable()->money('usd'),
            ])
            ->filters([
                Filter::make('created_at')
                    ->form([
                        Forms\Components\DatePicker::make('created_from'),
                        Forms\Components\DatePicker::make('created_until'),
                    ])
                    ->query(function ($query, array $data) {
                        return $query
                            ->when(
                                $data['created_from'],
                                fn($query) => $query->whereDate('created_at', '>=', $data['created_from'])
                            )
                            ->when(
                                $data['created_until'], fn($query) => $query->whereDate('created_at', '<=', $data['created_until'])
                            );
                    }),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPayments::route('/'),
        ];
    }

    public static function canCreate(): bool
    {
        return false;
    }

    public static function canDeleteAny(): bool
    {
        return false;
    }
}
